'use strict'

function setBrazilianPrices (price) {
    let prices = ''
    let site = window.location.hostname.split('.')

    if (site.includes('mercadolibre')) {
        prices = document.querySelectorAll('.price-tag-fraction, .price-fraction, .andes-money-amount__fraction, .mshops-card-variation-original__item-price')
    }
    else if (site.includes('shopee')) {
        prices = document.querySelectorAll('.ZEgDH9, .pmmxKx, .j3sDV8, .j0vBz2')
    } else {
        return false
    }
    
    chrome.storage.local.get(['cotacao_do_peso'], function(result) {
        let value = result.cotacao_do_peso
        let peso = value ? value : 55

        for (let i = 0; i < prices.length; i++) {
            let price = prices[i].innerText
            let real = price.split(',')[0].replace('$', '').replace('.', '') / peso
            let formated = real.toLocaleString('pt-br', {
                style: 'currency',
                currency: 'BRL'
            })
            prices[i].innerHTML = `${price} <small style="float: right; opacity: 0.5; position: relative; top: 3px; margin: 0 0 0 9px;">${formated}</small>`
        }
    })
}

setTimeout(function(){
    setBrazilianPrices()
}, 3000)
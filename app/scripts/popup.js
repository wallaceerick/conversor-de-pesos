'use strict'

chrome.storage.local.get(['cotacao_do_peso'], function(result) {
    let value = result.cotacao_do_peso
    let peso = value ? value : 55
    document.querySelector('.js-current-value').value = peso
})

let button = document.querySelector('.js-save')
function updateCurrentValue() {

    let v = document.querySelector('.js-current-value').value
            
    chrome.storage.local.set({ 'cotacao_do_peso': v }, function() { 
        console.log(v, 'cotação foi atualizada com sucesso!')
    })
    
    // chrome.storage.local.get(['cotacao_do_peso'], function(result) {
    //     console.log(result.cotacao_do_peso, 'cotação atual do peso!')
    //     return result.cotacao_do_peso
    // })

    // chrome.runtime.sendMessage('cotacao_do_peso', function(response) {
    //     console.log('response', response)
    //     console.log(v, 'cotação foi enviada com sucesso!')
    // })

    chrome.tabs.query({ active: true, currentWindow: true }, function(tabs){   
        var current = tabs[0]
        chrome.tabs.reload(current.id)
    })
}

document.addEventListener('DOMContentLoaded', function() {
    button.addEventListener('click', updateCurrentValue)
})